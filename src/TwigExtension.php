<?php

namespace Drupal\intl_date;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Format dates in Twig templates using Intl.
 */
class TwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'intl_date.twig_extension';
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('intl_format_date', [IntlDate::class, 'format']),
      new TwigFilter('intl_format_date_pattern', [IntlDate::class, 'formatPattern']),
    ];
  }

}
